busy = [
    {
        'start' : '10:30',
        'stop' : '10:50'
    },
    {
        'start' : '18:40',
        'stop' : '18:50'    
    },
    {
        'start' : '14:40',
        'stop' : '15:50'
    },
    {
        'start' : '16:40',
        'stop' : '17:20'
    },
    {
        'start' : '20:05',
        'stop' : '20:20'
    }
]


def convert_hourminute_to_minute(hm: str) -> int:
    t = [int(x) for x in hm.split(':')]
    return t[0] * 60 + t[1]
    


def convert_minute_to_hourminute(m: int) -> str:
    h = str(m//60)
    m = str(m%60)
    h = h if len(h) > 1 else '0'+h
    m = m if len(m) > 1 else '0'+m
    return  f'{h}:{m}'


def get_new_interval(interval, minutes=30):
    if interval[0] + minutes > interval[1]:
        return []
    else:
        return [
            [interval[0], interval[0] + minutes],
            *get_new_interval([interval[0] + minutes, interval[1]])
        ]
        
        
def combine_intervals(intervals):
    result = []
    for interval in intervals:
        result = [*result, *get_new_interval(interval)]
    return result


def get_free_schedule(busy):

    busy_intervals_minutes = [
            [
                convert_hourminute_to_minute(time['start']),
                convert_hourminute_to_minute(time['stop']) 
            ]
            for time in busy
        ]

    free_intervals_minutes = [
        [0, 1440]
    ]

    for interval_busy in busy_intervals_minutes:
        for index, interval_free in enumerate(free_intervals_minutes.copy()):
            if (interval_busy[0] > interval_free[0] and
                interval_busy[1] < interval_free[1]):
                free_intervals_minutes.insert(index+1, [
                    interval_busy[1], interval_free[1]
                ])
                free_intervals_minutes[index][1] = interval_busy[0]
                break
                
    free_intervals_minutes = combine_intervals(free_intervals_minutes)
         
    result = []
    for interval in free_intervals_minutes:
        result.append({
            'start': convert_minute_to_hourminute(interval[0]),
            'stop': convert_minute_to_hourminute(interval[1])
        })
    
    return result

result = get_free_schedule(busy)
print(result)
